#include "Inotify.h"

Inotify::Inotify(string member)
{
  default_adr = "/tmp/signals/";
  mkdir(default_adr.c_str(), 7777);

  default_adr += member;
  default_adr += "/";
  mkdir(default_adr.c_str(), 7777);

  m_member = member;
}

int Inotify::add_watch()
{
  // initital inotify file descriptor
  inotifyFd = inotify_init();
  if(inotifyFd == -1)
  {
    perror("inotify_init error\n");
    return -1;
  }

  // set watch on wanted directory
  wd = inotify_add_watch(inotifyFd, default_adr.c_str(), IN_CREATE);
  if(wd == -1)
  {
    perror("inotify_add_watch error \n");
    return -1;
  }

  return 0;
}

int Inotify::wait_for(string sig)
{
  string sig_adr;
  sig_adr.clear();

  sig_adr += default_adr;
  sig_adr += sig;

  add_watch();

  while(1)
  {
    numRead = read(inotifyFd, buf, BUF_LEN);
    if(numRead == 0 || numRead == -1)
    {
      perror("read function error \n");
      return -1;
    }

    for(p = buf; p < buf + numRead; p += sizeof(struct inotify_event) + event->len)
    {
      event = (struct inotify_event *) p;
      if(event->len > 0)
          printf("<%s> signal which is a member of <%s> is called \n", event->name, m_member.c_str());

      if(remove(sig_adr.c_str()) != 0)
      {
        perror("can't remove signal\n");
        return -1;
      }
    }

    return 0;
  }

  perror("error! reached end of the wait_for() without getting signal!\n");
  return -1;
}

int Inotify::set_signal(string sig)
{
  string sig_adr;
  sig_adr.clear();

  sig_adr += default_adr;
  sig_adr += sig;

  FILE *ofile;
  ofile = fopen(sig_adr.c_str(), "w");

  if(ofile == NULL)
  {
    perror("can't create sig file\n");
    return -1;
  }

  if(fclose(ofile) != 0)
  {
    perror("can't close file\n");
    return -1;
  }

  return 0;
}

