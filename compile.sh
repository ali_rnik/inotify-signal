#!/bin/sh

g++ -c -g -Wall -Werror -fpic inotify.cpp

g++ -shared -o libsigno.so inotify.o -linotify

L='-L/home/ramezanian_a/Projects/sigLib'
rpath='-rpath=/home/ramezanian_a/Projects/sigLib'

g++ -Wall -g $L -Wl,$rpath sigDetector.cpp -o sigDetector -lsigno 
g++ -Wall -g $L -Wl,$rpath sigSender.cpp -o sigSender -lsigno

rm -rf *.o *.core


