#ifndef INOTIFY_H
#define INOTIFY_H

#include <iostream>
#include <string>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/inotify.h>

#define BUF_LEN (10 * (sizeof(struct inotify_event) + NAME_MAX + 1))

using namespace std;

class Inotify
{
private:

  int inotifyFd, wd;
  char buf[BUF_LEN] __attribute__ ((aligned(8)));
  ssize_t numRead;
  char *p;
  struct inotify_event *event;
  string default_adr;
  string m_member;

  int add_watch();

public:

  Inotify(string member);
  int wait_for(string sig);
  int set_signal(string sig);
};

#endif
