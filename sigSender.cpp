#include "Inotify.h"

int main(int argc, char **argv)
{
  if(argc != 3)
  {
    cout << "\nIncorrect format, This program is a signal Sender!\n";
    cout << "Give 2 arguments to program!\n";
    cout << "Usage ./program member signal.\n\n";

    return 0;
  }

  Inotify sig_mgr(argv[1]);

  if((sig_mgr.set_signal(argv[2])) == -1)
    exit(EXIT_FAILURE);

  cout << "\nSignal <" << argv[2] << ">  which is a member of <" << argv[1] << "> sent successfully.\n";
  cout << "Program seccussfuly exited.\n\n";
}
