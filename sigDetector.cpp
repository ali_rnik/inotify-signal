#include "Inotify.h"

int main(int argc, char **argv)
{
  if(argc != 3)
  {
    cout << "\nIncorrect format, This program is a signal detector!\n";
    cout << "Give 2 arguments to program!\n";
    cout << "Usage ./program member signal.\n\n";

    return 0;
  }

  cout << "\nWaiting recieving signal ...\n";

  Inotify sig_mgr(argv[1]);

  if((sig_mgr.wait_for(argv[2])) == -1)
    exit(EXIT_FAILURE);

  cout << "Program seccussfuly exited.\n\n";
}
